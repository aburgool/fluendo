from django.conf.urls import url

from . import views

app_name = 'flutodo'
urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^login/$', views.login, name="login"),
    url(r'^register/$', views.register, name="register"),
    url(r'^logout/$', views.logout, name="logout"),

    url(r'^todos/$', views.todos, name="todos"),
    url(r'^create/$', views.create, name='create'),
    url(r'^delete/(?P<pk>[0-9]+)$', views.delete, name='delete'),
    url(r'^detail/(?P<pk>[0-9]+)$', views.detail, name='detail'),
    url(r'^done/(?P<pk>[0-9]+)$', views.done, name='done'),

    url(r'^api/todos/(?P<pk>[0-9]+)$', views.todo_list, name='todo_list'),
    url(r'^api/todo/$', views.todo_post, name='todo_post'),
    url(r'^api/todo/(?P<pk>[0-9]+)$', views.todo_api, name='todo_api'),

]

