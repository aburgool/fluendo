from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.http import HttpResponse

from rest_framework import serializers
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Todo
from .forms import UserForm, TodoForm
from .serializers import TodoSerializer


def index(request):
        return render(request, 'flutodo/index.html')

def login(request):
    if request.method == "POST":
        userlogin = request.POST['login']
        password = request.POST['password']
        user = authenticate(username=userlogin, password=password)
        if user is not None:
            auth_login(request, user)
            return redirect('flutodo:todos')
        else:
            pass

    return render(request, 'flutodo/login.html')


def register(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.set_password(request.POST['password'])
            new_user.save()
            messages.success(request, 'user ok you may login now')
            return redirect('flutodo:login')
        else:
            messages.error(request, 'register error: check your input')
    else:
        form = UserForm()

    return render(request, 'flutodo/register.html', {'form': form})


def logout(request):
    auth_logout(request)
    return redirect('flutodo:index')


def todos(request):
    if request.user is not None and request.user.is_authenticated:
        todos = Todo.objects.filter(user=request.user).order_by('created')
        context = {'todos': todos}
        return render(request, 'flutodo/todos.html', context)
    else:
        return redirect('flutodo:index')

def create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo = form.save(commit=False)
            todo.user = request.user
            todo.save()
            return redirect('flutodo:todos')
        else:
            return HttpResponse("error")
    else:
        form = TodoForm()

    return render(request, 'flutodo/create.html', {'form': form})

def detail(request, pk):
    todo_item = Todo.objects.get(pk=pk)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo = form.save(commit=False)
            todo.user = request.user
            todo.save()
            return redirect('flutodo:todos')
        else:
            return HttpResponse("error")
    else:
        form = TodoForm(instance=todo_item)

    return render(request, 'flutodo/detail.html', {'form': form})


def delete(request, pk):
    todo_item = Todo.objects.get(pk=pk)
    todo_item.delete()
    messages.success(request, 'todo item %s has been deleted' % todo_item.title)
    return redirect('flutodo:todos')


def done(request, pk):
    todo_item = Todo.objects.get(pk=pk)
    todo_item.done = not todo_item.done
    todo_item.save()
    return redirect('flutodo:todos')


@csrf_exempt
@api_view(['GET'])
def todo_list(request, pk):
    user = User.objects.filter(pk=int(pk))
    todo_list = Todo.objects.filter(user=user)
    todo_serializer = TodoSerializer(todo_list, many=True)
    return JsonResponse(todo_serializer.data, safe=False)


@csrf_exempt
@api_view(['POST'])
def todo_post(request):
    if request.method == 'POST':
        serializer = TodoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serilizer.errors, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(['GET', 'PUT', 'DELETE'])
def todo_api(request, pk):
    try:
        todo = Todo.objects.get(pk=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TodoSerializer(todo)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = TodoSerializer(todo, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serilizer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        todo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


