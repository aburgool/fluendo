from __future__ import unicode_literals

from django.utils import timezone
from django.db import models

from django.contrib.auth.models import User

class Todo(models.Model):
    title = models.CharField(max_length=255, null=True)
    created = models.DateTimeField('created', default=timezone.now)
    done = models.BooleanField(default=False)
    user = models.ForeignKey(User)

